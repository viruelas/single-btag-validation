#!/usr/bin/env python3

"""
Make a website from a top level directory
"""

from argparse import ArgumentParser
from pathlib import Path
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import SubElement as SE

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('root', type=Path)
    return parser.parse_args()

def make_indices(path):
    subdirs = {}
    images = {}
    for im in path.glob('*/*.pdf'):
        png = im.with_suffix('.png')
        images[im] = png if png.exists() else None
    for d in path.iterdir():
        if d.is_dir():
            subdirs[d] = make_indices(d)

    build_index(path, images, subdirs)
    return sum(subdirs.values()) + len(images)

def build_index(path, images, subdirs):
    by_dir = {}
    for pdf, png in images.items():
        by_dir.setdefault(pdf.stem,{})[pdf] = png
    plots = []
    for stem, idict in by_dir.items():
        plots.append( (stem, sorted((x,y) for x,y in idict.items()) ) )
    index = path/'index.html'
    root = ET.Element('html')
    parent = SE(root, 'a')
    parent.attrib['href'] = '..'
    parent.text = 'parent'
    tab = SE(root, 'table')
    for stem, row_plots in sorted(plots):
        row = SE(tab, 'tr')
        row.tail = '\n'
        for pdf, png in row_plots:
            local_link = pdf.relative_to(path)
            td = SE(row, 'td')
            SE(td, 'a').text = local_link.stem
            SE(td, 'br')
            a = SE(td, 'a')
            im = SE(a, 'img')
            imstr = png.relative_to(path).as_posix() if png else 'none'
            im.attrib['src'] = imstr
            a.attrib['href'] = local_link.as_posix()

    tab = SE(root, 'table')
    tab.tail = '\n'
    for dpath, nplots in subdirs.items():
        row = SE(tab, 'tr')
        row.tail = '\n'
        td = SE(row, 'td')
        dl = SE(td, 'a')
        subdir = dpath.relative_to(path).name
        dl.attrib['href'] = subdir
        dl.text = f'{subdir}: {nplots} figures'

    footer = SE(root,'center')
    link = SE(SE(footer, 'p'),'a')
    sbv = 'https://gitlab.cern.ch/dguest/single-btag-validation'
    link.attrib['href'] = sbv
    link.text = 'Source code in gitlab'
    et = ET.ElementTree(root)

    with open(index,'w') as ifile:
        et.write(ifile, encoding='unicode')

def run():
    args = get_args()
    make_indices(args.root)


if __name__ == '__main__':
    run()

