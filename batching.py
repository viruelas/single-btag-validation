import numpy as np

def yield_batches(ds, batch_size=10_000, fields=[], max_entries=None):
    """Generator that will return batches

    Note that this will overwrite the array on each iteration, make
    sure you copy it if before the iteration ends.

    """
    keep = set(ds.dtype.names)
    if selected := set(fields):
        if missing := selected - keep:
            raise ValueError("missing fields: {}".format(', '.join(missing)))
        keep = selected
    dtype = [(n, x) for n, x in ds.dtype.descr if n in keep]
    # start with an empty array of zero size, later we check to
    # make sure it fits the batch.
    ar = np.empty(0, dtype=dtype)
    ntot = ds.shape[0]
    max_size = ntot if max_entries is None else min(ntot, max_entries)
    for low in range(0, max_size, batch_size):
        high = min(low + batch_size, max_size)
        newshape = (high - low,) + ds.shape[1:]
        ar.resize(newshape, refcheck=False)
        ds.read_direct(ar, np.s_[low:high])
        yield ar
