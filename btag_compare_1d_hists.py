#!/usr/bin/env python3

"""
Compare multiple histograms side by side
"""


from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
import itertools
import json

from matplotlib.figure import Figure

from plotting import make_purty, get_spec, add_hist

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('hists', nargs='+', type=Path)
    parser.add_argument('-o', '--out-dir', type=Path,
                        default=Path('plots/compare-hists'))
    parser.add_argument('-s', '--hist-spec', type=Path,
                        default=Path('histspec.json'))
    parser.add_argument('-e', '--ext', nargs='+', default=['.pdf'])
    return parser.parse_args()

def draw_comp_plots(groups, fnames, path, specgroup, log=False, ext='.pdf'):
    fig = Figure((4,3))
    ax = fig.add_subplot()
    spec = None
    for name, group in zip(fnames,groups):
        ahist = group['all']
        if ahist['histogram'][1:-1].sum() == 0:
            print(f'skipping {name}, {path}, no data to plot')
            return
        if spec is None:
            spec = get_spec(ahist, path.stem, specgroup)
        add_hist(ax, ahist, spec, label=name)
    make_purty(ax, spec, log)
    path.parent.mkdir(parents=True, exist_ok=True)
    fig.canvas.print_figure(path.with_suffix(ext), bbox_inches='tight')

def draw_plots(groups, fnames, path, spec, ext):
    akey = 'type'
    print(f'plotting {path}')
    names = set()
    for f in groups:
        names |= set(f.keys())
    for name in names:
        subspec = spec.setdefault(name, {})
        subs = [f[name] for f in groups]
        opts = dict(specgroup=subspec, ext=ext)
        if subs[0].attrs.get(akey) == 'flavors':
            draw_comp_plots(subs, fnames, path/'linear'/name, **opts)
            draw_comp_plots(subs, fnames, path/'log'/name, **opts, log=True)
        else:
            draw_plots(subs, fnames, path/name, subspec, ext)

def run():
    args = get_args()
    if args.hist_spec.exists():
        with open(args.hist_spec) as hsfile:
            spec = json.load(hsfile)
    else:
        spec = {}

    files = [File(x) for x in args.hists]
    for ext in args.ext:
        draw_plots(files, args.hists, args.out_dir, spec, ext)

    with open(args.hist_spec,'w') as hsfile:
        json.dump(spec, hsfile, indent=2)


if __name__ == '__main__':
    run()
